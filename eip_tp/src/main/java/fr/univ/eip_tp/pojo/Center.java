
package fr.univ.eip_tp.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cages",
    "position",
    "name"
})
public class Center {

    @JsonProperty("cages")
    private List<Cage> cages = null;
    @JsonProperty("position")
    private Position_ position;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cages")
    public List<Cage> getCages() {
        return cages;
    }

    @JsonProperty("cages")
    public void setCages(List<Cage> cages) {
        this.cages = cages;
    }

    @JsonProperty("position")
    public Position_ getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(Position_ position) {
        this.position = position;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
