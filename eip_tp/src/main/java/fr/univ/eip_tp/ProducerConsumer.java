package fr.univ.eip_tp;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univ.eip_tp.pojo.Center;

public class ProducerConsumer {
	public static void main(String[] args) throws Exception {
		// Configure le logger par défaut
		BasicConfigurator.configure();
		
		// Contexte Camel par défaut
		CamelContext context = new DefaultCamelContext();
		
		// Crée une route contenant le consommateur
		RouteBuilder routeBuilder = new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				// On définit un consommateur 'consumer-1'
				// qui va écrire le message
				/**
				 * Question 1
				 * On obtient la réponse dans la commande
				 */
				from("direct:consumer-1").to("log:affiche-1-log");
				
				/**
				 * Questions 2
				 * Pour consumer-2, on reçoit la réponse dans le dossier ~/eip-tp/messages
				 * Pour consumer-all, le comportement est celui défini dans le sujet
				 */
				from("direct:consumer-2").to("file:messages");
				
				from("direct:consumer-all").choice()
					.when(header("destinataire").isEqualTo("écrire"))
						.to("direct:consumer-2")
					.otherwise()
						.to("direct:consumer-1");
				
				/**
				 * Questions 3
				 * MyServiceTP se trouve dans le projet ZOO_MANAGER à la racine du repository git
				 * 
				 * Le comportement de cette route est légèrement différente du sujet, étant donné que nos MyService ne gère pas des centres mais des cages dans les centres
				 * La route fonctionne ainsi : 
				 * -Si l'on a entré le nom d'un animal, celui-ci va être recherché dans le centre et nous sera renvoyé
				 * -Si l'on a entré le nom d'un animal, mais qu'il n'existe pas dans le centre, le serveur MyService va lancer une exception et s'arrêter
				 * -Si l'on n'a pas entré de nom, l'application va appeler "http://127.0.0.1:8084/animals" en GET, puis on envoie une requête vers geonames avec les données receptionnées 
				 */
				from("direct:zoo-manager").choice()
					.when(header("name").isNotEqualTo(""))
						.setHeader(Exchange.HTTP_METHOD,constant("GET"))
						.toD("http://127.0.0.1:8084/find/byName/${header.name}")
						.log("reponse received from zoo-manager : ${body}")
					.when(header("name").isEqualTo(""))
						.setHeader(Exchange.HTTP_METHOD,constant("GET"))
						// http://127.0.0.1:8084/
						// /find/byName/{name}
						.recipientList(simple("http://127.0.0.1:8084/animals"))
						.process((exchange) -> {
							String body = exchange.getIn().getBody(String.class);
							
							ByteArrayInputStream bais = new ByteArrayInputStream(body.getBytes());
							Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bais);
							
							NodeList list = doc.getElementsByTagName("position");
							org.w3c.dom.Node pos = list.item(list.getLength() - 1);
							
							exchange.getIn().setHeader("lat", pos.getFirstChild().getTextContent());
							exchange.getIn().setHeader("long", pos.getLastChild().getTextContent());
						})
						.recipientList(simple("http://api.geonames.org/findNearby?lat=${header.lat}&lng=${header.long}&username=m1gil"))
						.log("reponse received from geonames : ${body}");
				
				/**
				 * Question 4
				 * MyServiceTP1 se trouve dans le projet ZOO_MANAGER à la racine du repository git
				 * MyServiceTP2 se trouve dans le projet ZOO_MANAGER à la racine du repository git
				 * MyServiceTP3 se trouve dans le projet ZOO_MANAGER à la racine du repository git
				 * 
				 * La route fait 3 appels séquentiels à "http://127.0.0.1:808X/animals" en GET et va, pour chaque séquence, fusionner la nouvelle réponse avec l'ancienne
				 * 
				 * http://127.0.0.1:808X/animals -> X = 1, 2 ou 3 selon le nom du service
				 */
				from("direct:zoo-manager-multi")
					.setHeader(Exchange.HTTP_METHOD,constant("GET"))
					.recipientList(simple("http://127.0.0.1:8081/animals"))
					.process((exchange) -> {
						exchange.getIn().setHeader("xml", exchange.getIn().getBody(String.class));
					})
					.recipientList(simple("http://127.0.0.1:8082/animals"))
					.process((exchange) -> {
						//Reception du xml stockée dans le header "xml" et dans le body reçu
						String head = (String) exchange.getIn().getHeader("xml");
						String body = exchange.getIn().getBody(String.class);
						
						//string to document
						ByteArrayInputStream bais = new ByteArrayInputStream(head.getBytes());
						Document doc1 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bais);
						Node docNode = doc1.getDocumentElement();
						
						bais = new ByteArrayInputStream(body.getBytes());
						Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bais);
						//doc manipulations
						NodeList list = doc.getElementsByTagName("cages");
						//for each cage in list
						for (int i = list.getLength()-1 ; i >= 0 ; i--) {
							Node newCage = doc1.importNode(list.item(i), true);
							docNode.insertBefore(newCage, docNode.getFirstChild());
						}
						
						//document to string
						StringWriter writer = new StringWriter();
						Transformer transformer = TransformerFactory.newInstance().newTransformer();
						transformer.transform(new DOMSource(docNode), new StreamResult(writer));
						String xml = writer.toString();
						
						//Remise dans le header "xml"
						exchange.getIn().setHeader("xml", xml);
					})
					.recipientList(simple("http://127.0.0.1:8083/animals"))
					.process((exchange) -> {
						String head = (String) exchange.getIn().getHeader("xml");
						String body = exchange.getIn().getBody(String.class);
						
						ByteArrayInputStream bais = new ByteArrayInputStream(head.getBytes());
						Document doc1 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bais);
						Node docNode = doc1.getDocumentElement();
						
						bais = new ByteArrayInputStream(body.getBytes());
						Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bais);
						NodeList list = doc.getElementsByTagName("cages");
						//for each cage in list
						for (int i = list.getLength()-1 ; i >= 0 ; i--) {
							Node newCage = doc1.importNode(list.item(i), true);
							docNode.insertBefore(newCage, docNode.getFirstChild());
						}
						
						StringWriter writer = new StringWriter();
						Transformer transformer = TransformerFactory.newInstance().newTransformer();
						transformer.transform(new DOMSource(docNode), new StreamResult(writer));
						String xml = writer.toString();
						
						exchange.getIn().setHeader("xml", xml);
					})
					.log("${header.xml}");
				
				/**
				 * Question 5
				 * La même chose que la question 4, mais avec du JSON au lieu du XML
				 * 
				 * MyServiceController1 se trouve dans le projet ZOO_MANAGER-BOOT à la racine du repository git
				 * MyServiceController2 se trouve dans le projet ZOO_MANAGER-BOOT à la racine du repository git
				 * MyServiceController3 se trouve dans le projet ZOO_MANAGER-BOOT à la racine du repository git
				 * 
				 * La route fait 3 appels séquentiels à "http://127.0.0.1:808X/animals" en GET et va, pour chaque séquence, fusionner la nouvelle réponse avec l'ancienne
				 * 
				 * http://127.0.0.1:808X/animals -> X = 5, 6 ou 7 selon le nom du service
				 */
				from("direct:zoo-manager-multi-json")
					.setHeader(Exchange.HTTP_METHOD,constant("GET"))
					.recipientList(simple("http://127.0.0.1:8085/animals"))
					.process((exchange) -> {
						exchange.getIn().setHeader("json", exchange.getIn().getBody(String.class));
					})
					.recipientList(simple("http://127.0.0.1:8086/animals"))
					.process((exchange) -> {
						String head = (String) exchange.getIn().getHeader("json");
						String body = exchange.getIn().getBody(String.class);
						
						//Utilisation de POJO
						ObjectMapper mapper = new ObjectMapper();
						Center headCenter = mapper.readValue(head, Center.class);
						Center newCenter = mapper.readValue(body, Center.class);
						
						headCenter.getCages().addAll(newCenter.getCages());
						
						exchange.getIn().setHeader("json", mapper.writeValueAsString(headCenter));
					})
					.recipientList(simple("http://127.0.0.1:8087/animals"))
					.process((exchange) -> {
						String head = (String) exchange.getIn().getHeader("json");
						String body = exchange.getIn().getBody(String.class);
						
						//Utilisation de POJO
						ObjectMapper mapper = new ObjectMapper();
						Center headCenter = mapper.readValue(head, Center.class);
						Center newCenter = mapper.readValue(body, Center.class);
						
						headCenter.getCages().addAll(newCenter.getCages());
						
						exchange.getIn().setHeader("json", mapper.writeValueAsString(headCenter));
					})
					.log("${header.json}");
			}
		};
		
		// On ajoute la route au contexte
		routeBuilder.addRoutesToCamelContext(context);
		
		// On démarre le contexte pour activer les routes
		context.start();
		
		// On crée un producteur
		ProducerTemplate pt = context.createProducerTemplate();
		
		Scanner sc = new Scanner(System.in);
		
		/**
		 * Question 1
		 * J'ai laissé la fonction d'accès à consumer-1
		 */
		System.out.println("Start of program1");
		System.out.println("direct:consumer-1 -> Enter 'exit' to go to next program");
		String w = sc.nextLine();
		// qui envoie un message au consommateur 'consumer-1'
		while (! w.equals("exit")) {
			System.out.println("direct:consumer-1 -> Enter 'exit' to go to next program");
			pt.sendBody("direct:consumer-1", w);
			w = sc.nextLine();
		}
		System.out.println("End of program1");
		
		/**
		 * Questions 2
		 * Ici, on accède à consummer-all
		 */
		System.out.println("Start of program2 -> Enter a word that starts with 'w' letter to write in file. Else, write in log.");
		w = sc.nextLine();
		String ecrireBool = "";
		if (w.startsWith("w")) {
			ecrireBool = "écrire";
		}
		pt.sendBodyAndHeader("direct:consumer-all", 
				w,
				"destinataire", ecrireBool);
		System.out.println("End of program2");

		/**
		 * Questions 3
		 * Le comportement est décrit dans le commentaire laissé l.62
		 */
		System.out.println("Start of program3 -> enter nothing to search center in geonames, else find animal == what you write in zoo-manager");
		w = sc.nextLine();
		pt.sendBodyAndHeader("direct:zoo-manager", 
				"",
				"name", w);
		System.out.println("End of program3");
		
		/**
		 * Question 4
		 * Le comportement est décrit dans le commentaire laissé l.97
		 */
		System.out.println("Start of program4 -> enter nothing");
		w = sc.nextLine();
		pt.sendBodyAndHeader("direct:zoo-manager-multi", 
				"",
				"name", w);
		System.out.println("End of program4");
		
		/**
		 * Question 5
		 * Le comportement est décrit dans le commentaire laissé l.170
		 */
		System.out.println("Start of program5 -> enter nothing");
		w = sc.nextLine();
		pt.sendBodyAndHeader("direct:zoo-manager-multi-json", 
				"",
				"name", w);
		System.out.println("End of program5");
		
		
		sc.close();
	}
}