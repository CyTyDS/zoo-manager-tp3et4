﻿Le dossier eip_tp est le dossier principal du dossier.
Il contient le fichier ProducerConsumer.java utile pour notre évaluation,
et les POJO utiles à l'execution de celui-ci dans le package pojo.


Le dossier ZOO_MANAGER est notre projet du TP1&2.
Il a déjà été envoyé lors du TP1&2.
Il y a 4 "MyServiceTP.java", un par port : 
 * MyServiceTP -> port 8084
 * MyServiceTP1 -> port 8081
 * MyServiceTP2 -> port 8082
 * MyServiceTP3 -> port 8083
Rappel : notre ZOO-MANAGER ne gère pas de listes de centres,
mais des listes de cages. Nous n'aurons donc pas d'opérations sur
les centres, mais sur des cages.


Le dossier ZOO_MANAGER-BOOT est le service SpringBoot demandé.
Il effectue les opérations nécessaires au sujet de TP.
Il y a 3 Controllers, un par port :
 * MyServiceController1 -> port 8085
 * MyServiceController2 -> port 8086
 * MyServiceController3 -> port 8087

Pour executer notre ProducerConsumer.java, vous devez tous d'abord
executer tous les Services énoncés précédemment (commande "run" sous eclipse par exemple).
Une fois celà fait, vous pouvez executer ProducerConsumer.java.
Le programme affiche dans la console les réponses des services.
Le programme a été fait de façon séquentielle : 
il y a un premier Scanner qui offre une réponse à la première question du sujet de TP,
puis la deuxième, etc.

S'il y a un problème, n'hésitez pas à nous envoyer un mail sur nos adresses universitaires.
