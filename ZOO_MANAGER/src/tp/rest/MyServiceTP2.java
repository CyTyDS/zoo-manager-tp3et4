package tp.rest;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP2 implements Provider<Source> {

    public final static String url = "http://127.0.0.1:8082/";

    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP2());

        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }

    private JAXBContext jc;

    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

    public MyServiceTP2() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }

        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Toc", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tuc", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
    }

    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

        // determine the targeted ressource of the call
        // C'EST COMME CA QU'ON MET DES TARGETS
        try {
            // no target, throw a 404 exception.
            if (path == null) {
                throw new HTTPException(404);
            }
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
			//DONE
            else if (path.startsWith("animals")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                        return this.animalsCrud(method, source);
                    case 2 :
                        return this.animalCrud(method, source, path_parts[1]);
                    case 3 :
                    	return this.animalCrud(method, source, path_parts[1], path_parts[2]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if (path.startsWith("cages")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                        return this.cagesCrud(method, source);
                    case 2 :
                        return this.cagesCrud(method, source, path_parts[1]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if (path.startsWith("find")) {
            	String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 3 :
                        return this.findCrud(method, source, path_parts[1], path_parts[2]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if (path.startsWith("center/journey/from")) {
            	String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 4 :
                        return this.centerCrud(method, source, path_parts[3]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if ("coffee".equals(path)) {
                throw new HTTPException(418);
            }
            else {
                throw new HTTPException(404);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
    }
    
    // /center/journey/from/{position}
    private Source centerCrud(String method, Source source, String position) throws JAXBException {
    	if("GET".equals(method)){
        	//TODO TO TEST
    		//Fait des choses avec l'API GraphHopper
    		String[] splitted = position.split(",");
			String lat = splitted[0];
			String lon = splitted[1];
    		
    		QName qname = new QName("", "");
            Service service = Service.create(qname);
            service.addPort(qname, HTTPBinding.HTTP_BINDING,
                    "https://graphhopper.com/api/1/route?point=" + this.center.getPosition().getLatitude()
                            + "," + center.getPosition().getLongitude() + "&point=" + lat + "," + lon
                            + "&vehicle=car&locale=fr&key=f9a7c76b-3bcf-4ae1-9ccb-33a922fe0334&type=gpx");

            Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
            Map<String, Object> requestContext = dispatcher.getRequestContext();
            requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");

            return dispatcher.invoke(new JAXBSource(jc, this.center));
        }
        else{
            throw new HTTPException(405);
        }
    }
    
    //TODO TOTEST
    // /find/{word}/{attr}
    private Source findCrud(String method, Source source, String word, String attr) throws JAXBException {
        if (word.equals("byName")) {
        	if("GET".equals(method)){
        		//TODO TO TEST
        		try {
					Animal ret = this.center.getCages()
					.stream()
					.map(Cage::getResidents)
					.flatMap(Collection::stream)
					.filter(animal -> attr.equals(animal.getName()))
					.findFirst()
					.orElseThrow(AnimalNotFoundException::new);
					
	                return new JAXBSource(this.jc, ret);
				} catch (AnimalNotFoundException e) {
					// Auto-generated catch block
					e.printStackTrace();
					throw new HTTPException(405);
				}
            }
            else{
                throw new HTTPException(405);
            }
        }
		else if (word.equals("at")) {
			if("GET".equals(method)){
            	//TODO TO TEST
				//Création de la position via la string attr
				String[] splitted = attr.split(",");
				String lat = splitted[0];
				String lon = splitted[1];
				Position posToFind = new Position(new Double(lat), new Double(lon));
				
				//Recherche
				for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
	        		Cage cage = it.next();
	        		if (cage.getPosition().equals(posToFind)) {
	        			return new JAXBSource(this.jc, cage);
	        		}
	        	}
                return new JAXBSource(this.jc, this.center);
            }
            else{
                throw new HTTPException(405);
            }
		}
		else if (word.equals("near")) {
			if("GET".equals(method)){
            	//TODO TO TEST
				//Création de la position via la string attr
				String[] splitted = attr.split(",");
				String lat = splitted[0];
				String lon = splitted[1];
				Position posToFind = new Position(new Double(lat), new Double(lon));
				
				//Recherche
				for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
	        		Cage cage = it.next();
	        		if (Math.abs(cage.getPosition().getLatitude() - posToFind.getLatitude()) <= 1
	        				&& Math.abs(cage.getPosition().getLongitude() - posToFind.getLongitude()) <= 1) {
	        			return new JAXBSource(this.jc, cage);
	        		}
	        	}
                return new JAXBSource(this.jc, this.center);
            }
            else{
                throw new HTTPException(405);
            }
		}
        else{
            throw new HTTPException(404);
        }
    }
    
    // /animals/{something}/wolf 
    private Source animalCrud(String method, Source source, String animal_id, String wolf) throws JAXBException {
        if (wolf.equals("wolf")) {
        	if("GET".equals(method)){
            	//TODO TO TEST
        		//Faire des choses avec l'API Wolfram
        		//https://api.wolframalpha.com/v2/query?input=[wordToSearch]&output=XML&appid=QJYW5T-8492R6VU39
        		
        		//GET ANIMAL_ID
        		Animal animal;
				try {
					animal = this.center.findAnimalById(UUID.fromString(animal_id));
				} catch (AnimalNotFoundException e) {
					// Auto-generated catch block
					e.printStackTrace();
					throw new HTTPException(404);
				}
				
				//API CALL
        		QName qname = new QName("", "");
        		Service service = Service.create(qname);
                service.addPort(qname, HTTPBinding.HTTP_BINDING, "https://api.wolframalpha.com/v2/query?input=" + animal.getSpecies().replaceAll(" ", "%20") + "&output=XML&appid=QJYW5T-8492R6VU39");
                Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
                Map<String, Object> requestContext = dispatcher.getRequestContext();
                requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
                return dispatcher.invoke(new JAXBSource(jc, new Animal()));
            }
            else{
                throw new HTTPException(405);
            }
        }
        else{
            throw new HTTPException(404);
        }
    }
    
    /**
     * Method bound to calls on /animals/{something}
     */
    private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
        if("GET".equals(method)){
            try {
                return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
        else if("POST".equals(method)){
            //TODO ToTest
        	Animal animal = unmarshalAnimal(source);
        	animal.setId(UUID.fromString(animal_id));
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        }
        else if("PUT".equals(method)){
            //TODO ToTest
			try {
				//On retire l'animal
				Animal toDelete = center.findAnimalById(UUID.fromString(animal_id));
				this.center.getCages()
				.stream()
				.filter(cage -> cage.getName().equals(toDelete.getCage()))
	            .findFirst()
	            .orElseThrow(() -> new HTTPException(404))
	            .getResidents()
	            .remove(toDelete);
				
				//...puis on ajoute le nouveau
				Animal toAdd = unmarshalAnimal(source);
				toAdd.setId(UUID.fromString(animal_id));
				this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(toAdd.getCage()))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .add(toAdd);
				
				return new JAXBSource(this.jc, this.center);
			} catch (AnimalNotFoundException e) {
				// Auto-generated catch block
				e.printStackTrace();
				throw new HTTPException(400);
			}
        }
        else if("DELETE".equals(method)){
            //TODO ToTest
        	try {
				Animal animal = center.findAnimalById(UUID.fromString(animal_id));
				Collection<Cage> cages = this.center.getCages();
				cages
				.stream()
				.filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .remove(animal);
				this.center.setCages(cages);
				return new JAXBSource(this.jc, this.center);
			} catch (AnimalNotFoundException e) {
				// Auto-generated catch block
				e.printStackTrace();
				return new JAXBSource(this.jc, this.center);
			}
        }
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
        if("GET".equals(method)){
            return new JAXBSource(this.jc, this.center);
        }
        else if("POST".equals(method)){
            Animal animal = unmarshalAnimal(source);
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        }
        else if("PUT".equals(method)){
            //Testée
        	//Cette methode a besoin d'une collection d'animaux. 
        	//On supprime tous les animaux, puis on rajoute ceux présents dans la collection.
        	
        	Cage cages = (Cage) this.jc.createUnmarshaller().unmarshal(source);
        	//DELETE
        	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
        		Cage cage = it.next();
        		cage.setResidents(new LinkedList<>());
        	}
        	//POST
        	for (Animal animal : cages.getResidents()) {
                this.center.getCages()
                        .stream()
                        .filter(cage -> cage.getName().equals(animal.getCage()))
                        .findFirst()
                        .orElseThrow(() -> new HTTPException(404))
                        .getResidents()
                        .add(animal);
        	}
        	return new JAXBSource(this.jc, this.center);
        }
        else if("DELETE".equals(method)){
            //Testée
        	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
        		Cage cage = it.next();
        		cage.setResidents(new LinkedList<>());
        	}
        	return new JAXBSource(this.jc, this.center);
        }
        else{
            throw new HTTPException(405);
        }
    }
    
    //Method bound to calls on /cages POST method
    //Ce service rajoute des cages en POST et en DELETE une en la vidant
    //Utile pour le myClient
    private Source cagesCrud(String method, Source source) throws JAXBException {
        if("POST".equals(method)){
        	Cage cages = (Cage) this.jc.createUnmarshaller().unmarshal(source);
        	if (cages.getResidents() == null) {
        		cages.setResidents(new LinkedList<>());
        	}
        	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
        		Cage cage = it.next();
        		if (cage.getName().equals(cages.getName())) {
        			return new JAXBSource(this.jc, this.center);
        		}
        	}
            this.center.getCages().addAll(Arrays.asList(cages));
            return new JAXBSource(this.jc, this.center);
        }
        else{
            throw new HTTPException(405);
        }
    }

    //Method bound to calls on /cages/{cageName} DELETE method
    //Ce service DELETE une cage en la vidant
    //Utile pour le myClient
    private Source cagesCrud(String method, Source source, String cageName) throws JAXBException {
        if("DELETE".equals(method)){
        	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
        		Cage cage = it.next();
        		if (cageName.equals(cage.getName())) {
        			cage.setResidents(new LinkedList<>());
        		}
        	}
            return new JAXBSource(this.jc, this.center);
        }
        else{
            throw new HTTPException(405);
        }
    }
    
    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
}
